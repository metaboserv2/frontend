import { defineStore } from 'pinia'

export const useMessageStore = defineStore('msg', {
    state: () =>  ({
        messages: new MessageQueue(),
    }),
    getters: {
        /**
         * Returns the number of messages currently in the queue.
         * @param state - Store state
         * @returns Size of message storage
         */
        size(state) {
            return state.messages.size();
        },
        /**
         * Deletes the oldest message and returns it (in order to show it).
         * @param state - Store state
         * @returns - Oldest message in the queue
         */
        get_message(state) {
            return state.messages.dequeue();
        },
        /**
         * Returns all messages
         * @param state - Store state
         * @returns Messages
         */
        values(state) {
            return state.messages.values();
        }
    },
    actions: {
        /**
         * Adds a new message to the queue.
         * @param m - Message
         */
        add_message (m: Message) {
            this.messages.enqueue(m);
        },
        /**
         * Deletes the oldest message without showing it.
         */
        delete_message () {
            this.messages.dequeue();
        },
        /**
         * Close all messages.
         */
        close_all (){
            for (let m=0; m < this.messages.size(); m++)
                this.messages.dequeue();
        }
    }
});

export interface Message {
    status?: number,
    msg?: string,
    body?: any,
}

export interface MessageAPIResult {
    body: any,
    msg: string,
    status: number,
}

export class MessageQueue {
    private storage: Message[] = [];
    constructor (private capacity: number = 10) {};

    values(): Array<Message> {
        return this.storage;
    }

    enqueue(m: Message): void {
        if (this.size() === this.capacity) {
            this.storage.shift();
        }
        this.storage.push(m);
    }

    dequeue(): Message | undefined {
        return this.storage.shift();
    }

    size(): number {
        return this.storage.length;
    }
}
