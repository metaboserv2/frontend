import { defineStore } from 'pinia'
import type { StudyData } from './studies';
import type { MetaboliteFilterRule, PhenotypeFilterRule } from '@/modules/query_module';

export const useQueryStore = defineStore('query', {
  state: () =>  ({
    query_studies: [] as StudyData[] | null,
    query_compounds: {},
    query_compound_selection: [] as string[],
    query_active_metabolites: [] as string[],
    query_phenotypes: null as ArbitraryObject | null,
    query_phenotype_filters: [] as PhenotypeFilterRule[],
    query_filters: null as MetaboliteFilterRule[] | null,
    query_results: {} as ArbitraryObject,
    query_row_data: [] as any[],
    query_plot_ids: {} as PlotIDs,
    query_units: [] as UnitMap[] | null,
  }),
  actions: {
      metaboliteSubset (m1: string, m2?: string): ArbitraryObject {
        if (!this.query_results || !Object.keys(this.query_results).length) {
          return {};
        }
      let result_set: ArbitraryObject = {};
      for (let study in this.query_results) {
        result_set[study] = {};
        for(let source in this.query_results[study]) {
            for(let metabolite in this.query_results[study][source]) {
                if (metabolite.toLowerCase() == m1.toLowerCase() || (m2 && metabolite.toLowerCase() == m2.toLowerCase())) {
                    if (!result_set[study][source]) result_set[study][source] = {};
                    result_set[study][source][metabolite] = this.query_results[study][source][metabolite];
                }
                if (metabolite.startsWith('PHENOTYPE_')) {
                    if (!result_set[study][source]) result_set[study][source] = {};
                    result_set[study][source][metabolite] = this.query_results[study][source][metabolite];
                }
            }
        }
      }
      return result_set;
    },
    unitForMetabolite (metabolite_id: string): string[]  {
      if (!this.query_units) {
          return [];
      } 
      let units_for_metabolite: string[] = [];
      for (const [substudy, units] of Object.entries(this.query_units)) {
          if ({}.hasOwnProperty.call(units.unit_map, metabolite_id)) {
              units_for_metabolite.push(units.unit_map[metabolite_id]);
          }
      }
      return [...new Set(units_for_metabolite)];
    },
  }
});





export interface PlotIDs {
  scatterplot?: string,
  histogram?: string,
  correlation?: string,
  qcplot_met?: string,
  qcplot_src?: string,
  spectra?: string,
}

export interface ArbitraryObject {
  [key: string]: any,
}

export interface UnitMap {
  study_id: string,
  unit_map: ArbitraryObject,
}



